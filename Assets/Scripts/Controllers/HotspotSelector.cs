using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HotspotTest {
    public class HotspotSelector : MonoBehaviour
    {
        private Hotspot[] _hotspots;
        [SerializeField] private UIPositionController _uiPositionController;
        [SerializeField] private InfoWindow _infoWindow;

        private void Awake()
        {
            _hotspots = FindObjectsOfType<Hotspot>();
        }

        private void OnEnable()
        {
            foreach(var hotspot in _hotspots)
            {
                hotspot.HotspotSelected += SelectHotspot;
            }
        }

        private void OnDisable()
        {
            foreach (var hotspot in _hotspots)
            {
                hotspot.HotspotSelected -= SelectHotspot;
            }
        }
        private void SelectHotspot(Hotspot hotspot)
        {
            _uiPositionController.SetHotspot(hotspot.transform);
            _infoWindow.UpdateInfoText(hotspot.Info);
        }
    }
}