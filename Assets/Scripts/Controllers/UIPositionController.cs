using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HotspotTest
{
    public class UIPositionController : MonoBehaviour
    {
        [SerializeField] private Canvas _uiCanvas;
        [Space(3)]
        [SerializeField] private InfoWindow _infoWindow;
        [SerializeField] private WindowCorner _corner;

        private Transform _hotspot;

        private Vector2 _hotspotScreenPosition;
        private Vector2 _canvasBounds;

        private void Start()
        {
            CalculateCanvasBounds();
        }

        private void Update()
        {
            if (_hotspot && IsHotspotInViewport())
            {
                _hotspotScreenPosition = Camera.main.WorldToScreenPoint(_hotspot.position);
                SetWindowActive(true);
                UpdatePositions();
            }
            else
            {
                SetWindowActive(false);
            }

        }

        private void UpdatePositions()
        {
            Vector2 newInfoWindowPosition;
            var newCornerPosition = _hotspotScreenPosition;

            if (IsWindowAboveHotspot())
            {
                newInfoWindowPosition = CalculateWindowPositionAboveHotspot();
                newCornerPosition.y += _corner.YOffset;
            }
            else
            {
                newInfoWindowPosition = CalculateWindowPositionUnderHotspot();
                newCornerPosition.y -= _corner.YOffset + _corner.Size.y;
            }

            newInfoWindowPosition = ClampPosition(newInfoWindowPosition);

            _infoWindow.SetPosition(newInfoWindowPosition);
            _corner.SetPosition(newCornerPosition);
        }

        private Vector2 CalculateWindowPositionAboveHotspot()
        {
            return CalculateWindowPositionWithoutHotspotOffset() + new Vector2(0, _infoWindow.HotspotOffset);
        }

        private Vector2 CalculateWindowPositionUnderHotspot()
        {       
            return CalculateWindowPositionWithoutHotspotOffset() - new Vector2(0, _infoWindow.HotspotOffset);
        }

        private Vector2 CalculateWindowPositionWithoutHotspotOffset()
        {
            return _hotspotScreenPosition - _infoWindow.AnchorsOffset;
        }

        private Vector2 ClampPosition(Vector2 position)
        {
            return new Vector2(Mathf.Clamp(position.x, 0, _canvasBounds.x), Mathf.Clamp(position.y, 0, _canvasBounds.y));
        }

        private void CalculateCanvasBounds()
        {
            var xMax = _uiCanvas.pixelRect.width - 2 * _infoWindow.AnchorsOffset.x;
            var yMax = _uiCanvas.pixelRect.height - _infoWindow.AnchorsOffset.y / 2 - _infoWindow.HotspotOffset;
            _canvasBounds = new Vector2(xMax, yMax);
        }

        private void SetWindowActive(bool isActive)
        {
            if (_infoWindow.gameObject.activeSelf != isActive || _corner.gameObject.activeSelf != isActive)
            {
                _infoWindow.gameObject.SetActive(isActive);
                _corner.gameObject.SetActive(isActive);
            }
        }

        private bool IsHotspotInViewport()
        {
            var point = Camera.main.WorldToViewportPoint(_hotspot.position);
            return point.x >= 0 && point.x <= 1 && point.y >= 0 && point.y <= 1 && point.z > 0.3f;
        }

        private bool IsWindowAboveHotspot()
        {
            return _hotspotScreenPosition.y < _uiCanvas.pixelRect.height - _infoWindow.Size.y;
        }

        public void SetHotspot(Transform hotspot)
        {
            _hotspot = hotspot;
        }
    }
}