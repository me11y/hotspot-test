using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace HotspotTest
{
    public class Hotspot : MonoBehaviour
    {
        [TextArea]
        [SerializeField] private string _info;
        public string Info => _info;

        public UnityAction<Hotspot> HotspotSelected;
        private void Update()
        {
            transform.LookAt(Camera.main.transform);
        }

        public void Select()
        {
            HotspotSelected?.Invoke(this);
        }
    }
}