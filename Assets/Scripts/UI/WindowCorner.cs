using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HotspotTest
{
    [RequireComponent(typeof(RectTransform))]
    public class WindowCorner : MonoBehaviour
    {
        [SerializeField] private float _yOffset;
        public Vector2 Size { get; private set; }

        private void Awake()
        {
            Size = GetComponent<RectTransform>().rect.size;
        }
        public void SetPosition(Vector2 position)
        {
            transform.position = position;
        }

        public float YOffset => _yOffset;


    }
}