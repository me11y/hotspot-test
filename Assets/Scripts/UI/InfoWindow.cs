using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HotspotTest
{
    [RequireComponent(typeof(RectTransform))]
    public class InfoWindow : MonoBehaviour
    {
        [SerializeField] private float _offset;
        [SerializeField] private Text _info;

        private RectTransform _rectTransform;
        
        private void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
        }
        public void SetPosition(Vector2 position)
        {
            _rectTransform.anchoredPosition = new Vector2(position.x, position.y);
        }

        public void UpdateInfoText(string text)
        {
            _info.text = text;
        }

        public Vector2 AnchorsOffset => new Vector2(_rectTransform.rect.width / 2, _rectTransform.rect.height / 2);

        public float HotspotOffset => _offset;

        public Vector2 Size => new Vector2(_rectTransform.rect.width, _rectTransform.rect.height);
    }
}